#include "tokenizer/tokenizer.hpp"


#include <fstream>
#include <span>
#include <filesystem>
#include <sstream>


void tokenize(std::filesystem::path path)
{
  wub::Tokenizer tokenizer;
  std::ifstream inputstream{path.c_str()};
  std::stringstream buffer;
  buffer << inputstream.rdbuf();
  tokenizer.tokenize(buffer.str());
}


auto main(int argc, char *argv[]) -> int
{
  std::span args(argv, argc);
  args = args.subspan(1);

  tokenize(args[0]);
}
