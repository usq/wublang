#include "parser/parser.hpp"
#include "tokenizer/tokenizer.hpp"
#include <cstddef>
#include <memory>

using namespace wub;

auto Parser::parse(Tokenizer &&tokenizer) -> std::unique_ptr<ASTNode> {
  tokenizer_ = std::move(tokenizer);
  return parse_prog();
}

/// prog : stmt*
auto Parser::parse_prog() -> std::unique_ptr<Prog> {
  std::vector<std::unique_ptr<ASTNode>> statements;
  for (auto statement = parse_stmt(); statement; statement = parse_stmt()) {
    statements.push_back(std::move(statement));
  }

  auto prog = std::make_unique<Prog>();
  prog->set_statements(std::move(statements));
  return prog;
};

///
// stmt : var_decl
//      | assign
//      | expr
auto Parser::parse_stmt() -> std::unique_ptr<ASTNode> {
  PosGuard pos{tokenizer_};
  auto var_decl = parse_var_decl();
  if (var_decl) {
    pos.dismiss();
    return var_decl;
  }
  return nullptr;
};

// var_decl : "var" IDENTIFIER;
auto Parser::parse_var_decl() -> std::unique_ptr<ASTNode> {
  PosGuard guard{tokenizer_};
  auto var_ident = tokenizer_.get();
  if (var_ident.lexeme != "var") {
    return nullptr;
  }
  const auto var_name = tokenizer_.get();
  const auto semicolon = tokenizer_.get();
  if (var_name.type == TokenType::IDENTIFIER &&
      semicolon.type == TokenType::SEMICOLON) {
    guard.dismiss();
    return std::make_unique<VarDecl>(std::move(var_ident), std::move(var_name));
  }
  return nullptr;
};

/// expr : binop | (expr)
auto Parser::parse_expr() -> std::unique_ptr<ASTNode> {
  // ( expr )
  PosGuard guard{tokenizer_};

  auto next = tokenizer_.get();

  if(next.type == TokenType::LPAREN)
  {
    auto expr = parse_expr();
    if(!expr)
    {
      return nullptr;
    }

    auto rparen = tokenizer_.get();
    if(rparen.type == TokenType::RPAREN)
    {
      guard.dismiss();
      return std::make_unique<Parens>(std::move(expr));
    }
    // failed to match
    return nullptr;
  }
}
