#pragma once

#include "tokenizer/tokenizer.hpp"
#include <cstddef>
#include <memory>
#include <type_traits>
#include <vector>

namespace wub {

class ASTVisitor;
class ASTNode {
public:
  virtual void accept(ASTVisitor &v) {}
  virtual ~ASTNode() = default;
};
class Stmt : public ASTNode {};
class Prog : public ASTNode {

public:
  void
  set_statements(std::vector<std::unique_ptr<ASTNode>> &&statements) noexcept(
      std::is_nothrow_move_assignable<
          std::vector<std::unique_ptr<ASTNode>>>::value) {
    statements_ = std::move(statements);
  }

private:
  std::vector<std::unique_ptr<ASTNode>> statements_;
};

class VarDecl : public ASTNode {

public:
  VarDecl(Token var_decl, Token var_name)
      : var_decl_(std::move(var_decl)), var_name_(std::move(var_name)) {}

private:
  Token var_decl_;
  Token var_name_;
};

class Assignment : public ASTNode {}; // a = b;
class Parens : public ASTNode {
public:
  explicit Parens(std::unique_ptr<ASTNode> &&child)
      : child_(std::move(child)) {}

private:
  std::unique_ptr<ASTNode> child_;
};
class Expr : public ASTNode {};
class BinOp : public ASTNode {}; // a + b

class ASTVisitor {
public:
  virtual void visit(ASTNode &node) {}
  virtual void visit(Stmt &node) {}
  virtual void visit(Prog &node) {}
  virtual void visit(VarDecl &node) {}
  virtual void visit(Assignment &node) {}
  virtual void visit(Expr &node) {}
  virtual void visit(BinOp &node) {}
};

/*

  var a; // var decl
  a = 4; // var assign
  a = 3 + 3;

   prog : stmt*

   stmt : var_decl
        | assign
        | expr
   var_decl : "var" IDENTIFIER;

  assign: IDENTIFIER "=" expr
  expr: binop | ( expr )
  binop: (identifier|number) op (identifier|number)
  op: "+" "-" "*" "/"
 */

class PosGuard {
public:
  PosGuard(const PosGuard &) = delete;
  PosGuard(PosGuard &&) = delete;
  auto operator=(const PosGuard &) -> PosGuard & = delete;
  auto operator=(PosGuard &&) -> PosGuard & = delete;

  PosGuard(Tokenizer &tokenizer)
      : tokenizer_(tokenizer), pos_(tokenizer.pos()) {}

  void dismiss() { active_ = false; }

  ~PosGuard() {
    if (active_) {
      tokenizer_.set_pos(pos_);
    }
  }

private:
  Tokenizer &tokenizer_;
  size_t pos_ = 0;
  bool active_ = true;
};

class Parser {

public:
  auto parse(Tokenizer &&tokenizer) -> std::unique_ptr<ASTNode>;

  /// prog : stmt*
  auto parse_prog() -> std::unique_ptr<Prog>;

  ///
  // stmt : var_decl
  //      | assign
  //      | expr
  auto parse_stmt() -> std::unique_ptr<ASTNode>;

  // var_decl : "var" IDENTIFIER;
  auto parse_var_decl() -> std::unique_ptr<ASTNode>;

  /// expr : binop | (expr)
  auto parse_expr() -> std::unique_ptr<ASTNode>;

private:
  Tokenizer tokenizer_;
};

} // namespace wub
