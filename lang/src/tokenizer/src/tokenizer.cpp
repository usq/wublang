#include "tokenizer/tokenizer.hpp"

using namespace wub;

// NOLINTNEXTLINE
Token::Token(std::string lexeme_, TokenType type_, size_t line_,
             size_t start_col_, size_t end_col_, int as_number_)
    : lexeme(std::move(lexeme_)), type(type_), line(line_),
      start_col(start_col_), end_col(end_col_), as_number(as_number_) {}

void Tokenizer::tokenize(std::string content) {
  content_ = std::move(content);

  std::string running;
  size_t line = 1;
  size_t col = 1;
  size_t col_start = 1;

  auto finalize = [this, &running, &line, &col, &col_start]() {
    if (!running.empty()) {
      try {
        auto number = std::stoi(running);
        tokens_.emplace_back(running, TokenType::NUMBER, line, col_start, col,
                             number);
      } catch (const std::invalid_argument &) {
        tokens_.emplace_back(running, TokenType::IDENTIFIER, line, col_start,
                             col);
      }
      running.clear();
    }
  };

  for (char character : content_) {
    if (character == '(') {
      finalize();
      tokens_.emplace_back("(", TokenType::LPAREN, line, col, col);
    } else if (character == ')') {
      finalize();
      tokens_.emplace_back(")", TokenType::RPAREN, line, col, col);
    } else if (character == '+') {
      finalize();
      tokens_.emplace_back("+", TokenType::PLUS, line, col, col);
    } else if (character == '=') {
      finalize();
      tokens_.emplace_back("=", TokenType::EQUAL, line, col, col);
    } else if (character == '\n') {
      finalize();
      line++;
      col = 1;
      continue;
    } else if (character == ' ') {
      finalize();
    } else {
      if (running.empty()) {
        col_start = col;
      }
      running += character;
    }
    col++;
  }
  finalize();
}

auto Tokenizer::get() -> Token {
  if (pos_ >= tokens_.size()) {
    return Token{"", TokenType::EOFT, 0, 0, 0};
  }
  return tokens_[pos_++];
}

auto Tokenizer::pos() const -> size_t {
  return pos_;
}

void Tokenizer::set_pos(size_t new_pos) {
  pos_ = new_pos;
}
