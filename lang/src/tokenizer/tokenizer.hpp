#pragma once

#include <string>
#include <tuple>
#include <vector>

namespace wub {

enum class TokenType {
  LPAREN,
  RPAREN,
  NUMBER,
  IDENTIFIER,
  PLUS,
  EQUAL,
  SEMICOLON,
  EOFT,
};

struct Token {
  // NOLINTNEXTLINE
  Token(std::string lexeme_, TokenType type_, size_t line_, size_t start_col_,
        size_t end_col_, int as_number_ = 0);
  std::string lexeme;
  TokenType type = TokenType::EOFT;
  size_t line = 0;
  size_t start_col = 0;
  size_t end_col = 0;
  int as_number = 0;

  auto operator==(const Token& other) const -> bool = default;
  auto operator!=(const Token &other) const -> bool = default;
};

class Tokenizer {
public:
  void tokenize(std::string content);
  auto get() -> Token;
  [[nodiscard]] auto pos() const -> size_t;
  void set_pos(size_t new_pos);

private:
  std::string content_;
  std::vector<Token> tokens_;
  size_t pos_ = 0;
};
} // namespace wub
