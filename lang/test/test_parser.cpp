#include "catch2/catch_test_macros.hpp"
#include "tokenizer/tokenizer.hpp"
#include "parser/parser.hpp"

using namespace wub;

// NOLINTNEXTLINE
TEST_CASE("test parser", "[parser]")
{
  Tokenizer tokenizer;
  Parser parser;

  SECTION("variable declaration") {
    tokenizer.tokenize("var a;");
    auto root_node = parser.parse(std::move(tokenizer));
    REQUIRE(root_node);
  }

  SECTION("variable assignment")
  {
    tokenizer.tokenize("a = 42;");
    auto root_node = parser.parse(std::move(tokenizer));
    REQUIRE(root_node);
  }
  SECTION("computation assignment")
  {
    tokenizer.tokenize("a = (2 + 4);");
    auto root_node = parser.parse(std::move(tokenizer));
    REQUIRE(root_node);
  }
}


