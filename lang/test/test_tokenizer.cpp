#include "catch2/matchers/catch_matchers.hpp"
#include "tokenizer/tokenizer.hpp"
#include "catch2/catch_test_macros.hpp"

using namespace wub;

// NOLINTNEXTLINE
TEST_CASE("test", "[module]")
{
  Tokenizer tokenizer;

  SECTION("numbers and parentheses")
  {
    tokenizer.tokenize("( 11 2)");
    REQUIRE(tokenizer.get().lexeme == "(");
    REQUIRE(tokenizer.get().lexeme == "11");
    REQUIRE(tokenizer.get().lexeme == "2");
    REQUIRE(tokenizer.get().lexeme == ")");
  }

  SECTION("basic literals")
  {
    tokenizer.tokenize("abc ab 11 ()");
    REQUIRE(tokenizer.get().lexeme == "abc");
    REQUIRE(tokenizer.get().lexeme == "ab");
    REQUIRE(tokenizer.get().lexeme == "11");
    REQUIRE(tokenizer.get().lexeme == "(");
    REQUIRE(tokenizer.get().lexeme == ")");
  }

  SECTION("basic arithmetic") {
    tokenizer.tokenize("abc 1+1=2 def");
    REQUIRE(tokenizer.get().lexeme == "abc");
    REQUIRE(tokenizer.get().lexeme == "1");
    REQUIRE(tokenizer.get().lexeme == "+");
    REQUIRE(tokenizer.get().lexeme == "1");
    REQUIRE(tokenizer.get().lexeme == "=");
    REQUIRE(tokenizer.get().lexeme == "2");
    REQUIRE(tokenizer.get().lexeme == "def");
  }

  SECTION("Distinguish identifiers and numbers")
  {
    tokenizer.tokenize("abc 123");
    Token abc = tokenizer.get();
    REQUIRE(abc.type == TokenType::IDENTIFIER);

    Token number = tokenizer.get();
    REQUIRE(number.type == TokenType::NUMBER);
  }


  SECTION("pos returns position, set_pos restores it")
  {
    tokenizer.tokenize("abc 123 def");
    REQUIRE(tokenizer.pos() == 0);
    tokenizer.get();
    auto pos1_pos = tokenizer.pos();
    REQUIRE(pos1_pos == 1);
    auto pos1_token = tokenizer.get();
    REQUIRE(tokenizer.pos() == 2);
    auto pos2_token = tokenizer.get();
    REQUIRE(pos2_token.lexeme == "def");
    REQUIRE(pos1_token != pos2_token);

    tokenizer.set_pos(pos1_pos);
    REQUIRE(tokenizer.get() == pos1_token);
  }
}


