#include <nlohmann/json.hpp>
#include <catch2/catch_test_macros.hpp>
#include <cstring>
#include <sstream>
#include "io.hpp"
#include "message.hpp"

using namespace wub;
using namespace nlohmann;

// NOLINTNEXTLINE
TEST_CASE("reader reads bytes", "[Reader]")
{
  std::stringstream input_stream;
  InputReader reader{input_stream};

  SECTION("header field")
  {
    std::string line = "Content-Size: 7\r\nline2: val2\r\n\r\ncontent";
    input_stream.write(line.c_str(), static_cast<long>(line.size()));

    Message message = reader.read();

    REQUIRE(message.content_size == 7);
    REQUIRE(message.content == "content");
  }

  SECTION("RPC request")
  {
    std::string minimal_request = "Content-Size: 48\r\n\r\n"
                                  R"({"jsonrpc":"2.0", "id":"42", "method":"unknown"})";
    input_stream.write(minimal_request.c_str(), static_cast<long>(minimal_request.size()));

    Message message = reader.read();
    REQUIRE(message.content.size() == message.content_size);
  }
}

// NOLINTNEXTLINE
TEST_CASE("Initialize Request Parsing")
{
  //using namespace nlohmann::literals;
  json request_json;
  request_json["id"] = 42;
  request_json["method"] = "initialize";
  request_json["jsonrpc"] = "2.0";
  request_json["params"] = {};

  Message message;
  message.content = request_json.dump();

  auto maybe_request = RequestFactory::from_content(message.content);
  REQUIRE(maybe_request);
  // NOLINTNEXTLINE
  auto request = std::move(*maybe_request);
  REQUIRE(request->id() == 42);
}
