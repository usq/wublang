#pragma once

#include "io.hpp"
#include "message.hpp"

#include <nlohmann/json.hpp>

#include <iostream>
#include <memory>
#include <optional>
#include <string_view>

namespace wub {
class Server {
 public:
  void serve() {

    InputReader reader{std::cin};

    while(true)
    {
      const auto message = reader.read();
      const auto parsedMessage = RequestFactory::from_content(std::string_view{message.content});
      if(parsedMessage)
      {
        auto response = handleMessage(parsedMessage.value());
        if(response)
        {
          nlohmann::json json_resp;
          (*response)->to_json(json_resp);
        }
      }
    }
  }

  auto handleMessage(const std::unique_ptr<RequestMessage>& message) -> std::optional<std::unique_ptr<ResponseMessage>>
  {
    return std::nullopt;
  }
};
} // namespace wub
