#pragma once

#include <optional>
#include <string>
#include <vector>

namespace wub {

struct Message {
  size_t content_size;
  std::string content;
};

class InputReader {

  struct HeaderField {
    std::string name;
    std::string value;
  };

public:
  explicit InputReader(std::istream &input_stream);
  [[nodiscard]] auto read() -> Message;

private:
  auto read_header_fields() -> std::vector<HeaderField>;
  void read_rn();
  auto read_header_field() -> std::optional<HeaderField>;
  [[nodiscard]] auto read_body(size_t size) const -> std::string;

  // Members
  std::istream &input_stream_;
};

class OutputWriter {
public:
  void write() {}
};
} // namespace wub
