#include "io.hpp"

#include <algorithm>
#include <cassert>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>

using namespace wub;

InputReader::InputReader(std::istream &input_stream)
    : input_stream_{input_stream} {
  std::noskipws(input_stream);
}

auto InputReader::read() -> Message {
  assert(input_stream_.peek() != '\r');
  assert(input_stream_.peek() != '\n');

  auto fields = read_header_fields();
  Message message{};
  std::for_each(fields.begin(), fields.end(), [&message](const auto& field){
    if(field.name == "Content-Size")
    {
      message.content_size = std::stoull(field.value);
    }
  });

  read_rn();
  message.content = read_body(message.content_size);
  return message;
}

auto InputReader::read_header_fields() -> std::vector<InputReader::HeaderField>
{
  std::vector<HeaderField> fields;
  for(auto field = read_header_field(); field; field = read_header_field())
  {
    fields.push_back(field.value());
  }
  return fields;
}

void InputReader::read_rn() {
  assert(input_stream_.get() == '\r');
  assert(input_stream_.get() == '\n');
}

auto InputReader::read_header_field() -> std::optional<InputReader::HeaderField> {
  if(input_stream_.peek() == '\r')
  {
    return std::nullopt;
  }
  std::string name;
  std::getline(input_stream_, name, ':');
  std::string value;
  input_stream_ >> std::skipws >> value >> std::noskipws;
  read_rn();
  return HeaderField{name, value};
}

auto InputReader::read_body(size_t size) const -> std::string
{
  assert(size > 0);
  std::vector<char> content_string;
  content_string.resize(size);
  std::copy_n(std::istream_iterator<char>(input_stream_), size, content_string.begin());
  return std::string{content_string.begin(), content_string.end()};
}

