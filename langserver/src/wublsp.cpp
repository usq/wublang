#include "server.hpp"

auto main(int argc, char* argv[]) -> int
{
  wub::Server server;
  server.serve();
}

