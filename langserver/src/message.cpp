#include "message.hpp"
#include "nlohmann/json.hpp"
#include "nlohmann/json_fwd.hpp"
#include <memory>
#include <optional>
#include <string>

using namespace wub;
using nlohmann::json;

auto RequestFactory::from_content(std::string_view content) -> std::optional<std::unique_ptr<RequestMessage>>
{
  const auto json_content = nlohmann::json::parse(content);
  if(json_content.find("method") == json_content.end())
  {
    return std::nullopt;
  }

  if (json_content["method"] == "initialize")
  {
    const auto identifier = json_content["id"];
    return std::make_unique<InitializeRequest>(identifier, json_content["params"]);
  }
  return std::nullopt;
}


auto ResponseMessage::to_json(nlohmann::json &json) const -> void
{
}

InitializeRequest::InitializeRequest(int identifier, const nlohmann::json &json)
    : RequestMessage(identifier)
{
}
