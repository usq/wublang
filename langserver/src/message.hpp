#pragma once

#include <nlohmann/json_fwd.hpp>

#include <memory>
#include <optional>
#include <string>
#include <string_view>


namespace wub {

class RequestMessage {
 public:
   explicit RequestMessage(int identifier) : id_(identifier) {}
   virtual ~RequestMessage() = default;

   RequestMessage(const RequestMessage &) = delete;
   RequestMessage(RequestMessage &&) = delete;
   auto operator=(const RequestMessage &) -> RequestMessage & = delete;
   auto operator=(RequestMessage &&) -> RequestMessage & = delete;

  [[nodiscard]] auto id() const -> int {
    return id_;
  }

 private:
   int id_;
};


class InitializeRequest: public RequestMessage
{
 public:
  InitializeRequest(int identifier, const nlohmann::json &json);
};

class RequestFactory {
 public:
  static auto from_content(std::string_view content) -> std::optional<std::unique_ptr<RequestMessage>>;
};

class ResponseMessage {
public:
  explicit ResponseMessage(int identifier) : id_(identifier) {}
  virtual ~ResponseMessage() = default;

  ResponseMessage(const ResponseMessage &) = delete;
  ResponseMessage(ResponseMessage &&) = delete;
  auto operator=(const ResponseMessage &) -> ResponseMessage & = delete;
  auto operator=(ResponseMessage &&) -> ResponseMessage & = delete;

  virtual void to_json(nlohmann::json &json) const;

private:
  int id_;
};

} // namespace wub
